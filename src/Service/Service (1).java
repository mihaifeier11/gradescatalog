package Service;

import Domain.Nota;
import Domain.Strategy;
import Domain.Student;
import Domain.Tema;
import Exceptions.ValidationException;
import Repository.CrudRepository;
import Repository.NotaFileRepository;
import Repository.StudentFileRepository;
import Repository.TemaFileRepository;
import Validator.StrategyValidator;
import Validator.VEValidator;
import javafx.util.Pair;

public class Service {
    CrudRepository<String, Student> studentRepo = new StudentFileRepository();
    CrudRepository<String, Tema> temaRepo = new TemaFileRepository();
    CrudRepository<String, Nota> notaRepo = new NotaFileRepository();
    Integer currentWeek = 0;

    public void setCurrentWeek(Integer currentWeek){
        this.currentWeek = currentWeek;
    }

    VEValidator studentValidator = new StrategyValidator().validator(Strategy.Student);
    VEValidator temaValidator = new StrategyValidator().validator(Strategy.Tema);
    public Object addStudent(String id, String nume, Integer grupa, String email, String cadruIndrumator) throws ValidationException {
        Student student = new Student(id, nume, grupa, email, cadruIndrumator);
        studentValidator.validate(student);
        return studentRepo.save(student);

    }
     public Object getStudent(String id){
        return studentRepo.findOne(id);
     }

     public Iterable<Student> getAllStudents() {
        return studentRepo.findAll();
     }

     public Object updateStudent(Student student) throws ValidationException {
        studentValidator.validate(student);
        return studentRepo.update(student);
     }

     public Object deleteStudent(String id) {
        return studentRepo.delete(id);
     }

     public Object addTema(String id, String descriere, Integer deadline, Integer dateReceived) throws ValidationException {
        Tema tema = new Tema(id, descriere, deadline, dateReceived);
        temaValidator.validate(tema);
        return temaRepo.save(tema);
     }

     public Object setDeadline(String id, Integer deadline) throws ValidationException {
        Tema tema = temaRepo.findOne(id);
        tema.setDeadline(deadline);
        temaValidator.validate(tema);
        return temaRepo.update(tema);
     }

    public Object getTema(String id){
        return temaRepo.findOne(id);
    }

    public Iterable<Tema> getAllTeme() {
        return temaRepo.findAll();
    }

    public Object addNota(String idStudent, String idTema, Integer valoare) throws ValidationException {
        Student student = studentRepo.findOne(idStudent);
        Tema tema = temaRepo.findOne(idTema);
        Integer deadline = tema.getDeadline();
        Nota nota = new Nota(new Pair<String, String>(idStudent, idTema), student, tema, valoare, this.currentWeek, deadline);

        return notaRepo.save(nota);
    }
}
