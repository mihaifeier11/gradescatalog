package Exceptions;
import java.io.*;

public class ValidationException extends Exception {
    /**
     * function prints the message given as parameter if the exception is thrown
     * @param message
     *      the message
     *      must be String
     */
    public ValidationException(String message){
        System.out.println(message);
    }
}
