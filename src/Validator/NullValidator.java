package Validator;

public class NullValidator implements IAEValidator{
    @Override
    public void validate(Object entity) throws IllegalArgumentException {
        if (entity == null) {
            throw new IllegalArgumentException("Id-ul nu poate fi null.");
        }
    }
}
