package Validator;

import Domain.Strategy;


public class StrategyValidator {

    public VEValidator validator (Strategy strategy) {
        if (strategy == Strategy.Student){
            return new StudentValidator();
        } else if (strategy == Strategy.Tema){
            return new TemaValidator();
        }
        return null;
    }
}
