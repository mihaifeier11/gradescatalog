package Validator;

import Domain.Nota;
import Domain.Student;
import Domain.Tema;
import Exceptions.ValidationException;
import Repository.CrudRepository;

public class NotaValidator implements VEValidator<Nota> {
    @Override
    public void validate(Nota entity) throws ValidationException {

    }

    public void validateNota(CrudRepository studentRepo, CrudRepository temaRepo, Nota nota) throws ValidationException {
        Tema tema = nota.getTema();
        Student student = nota.getStudent();

        if (studentRepo.findOne(student.getID()) == null){
            throw new ValidationException("Studentul nu exista");
        }

        if (temaRepo.findOne(tema.getID()) == null){
            throw new ValidationException("Tema  nu exista");
        }
        if (nota.getData() > tema.getDeadline() + 2) {
            throw new ValidationException("Tema nu mai poate fi predata, deadline-ul a fost depasit cu mai mult de 2 saptamani");
        }
    }
}
