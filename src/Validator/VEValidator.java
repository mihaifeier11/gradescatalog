package Validator;

import Exceptions.ValidationException;

public interface VEValidator<E> {
    /**
     *
     * @param entity
     * @throws ValidationException - if the given entity is not valid
     */
    void validate(E entity) throws ValidationException;
}

