package Validator;

import Exceptions.ValidationException;

import java.time.LocalDate;
import java.util.Date;

@SuppressWarnings("ALL")
public class DeadlineValidator implements VEValidator{
    @Override
    public void validate(Object entity) throws ValidationException {
        entity = (Date) entity;
        LocalDate currentDate = LocalDate.now();
        Date date = new Date(currentDate.getYear(), currentDate.getMonthValue(), currentDate.getDayOfMonth());
        if (((Date) entity).before(date)){
            throw new ValidationException("Deadline-ul nu poate fi mai mic decat data curenta.");
        }
    }
}
