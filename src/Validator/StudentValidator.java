package Validator;

import Domain.Student;
import Exceptions.ValidationException;

public class StudentValidator implements VEValidator {
    @Override
    public void validate(Object entity) throws ValidationException {
        if (!(entity instanceof Student)) {
            throw new ValidationException("Entitatea nu este student.");
        }
        Student student = (Student) entity;
        validateId(student.getID());
        validateName(student.getNume());
        validateEmail(student.getEmail());
        validateName(student.getCadruIndrumator());
    }

    public void validateId(String id) throws ValidationException {
        if (!id.matches("[A-Za-z0-9]+")){
            throw new ValidationException("Id-ul nu este alfanumeric.");
        }
    }

    public void validateName(String name) throws ValidationException {
        if (!name.matches("[A-Za-z ]+")){
            throw new ValidationException("Numele nu este format doar din litere si spatii.");
        }
    }

    public void validateEmail(String email) throws ValidationException {
        if (!email.matches("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,}[a-zA-Z0-9])?)*$")){
            throw new ValidationException("Email-ul nu este valid.");
        }
    }
}
