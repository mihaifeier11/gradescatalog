package Validator;

import Domain.Tema;
import Exceptions.ValidationException;

public class TemaValidator implements VEValidator {
    @Override
    public void validate(Object entity) throws ValidationException {
        if (!(entity instanceof Tema)){
            throw new ValidationException("Entitatea nu este tema.");
        }
        Tema tema = (Tema) entity;
        validateId(tema.getID());
        validateDescriere(tema.getDescription());
        validateDate(tema.getDeadline());
        validateDate(tema.getDateReceived());
        validateDeadline(tema.getDeadline(), tema.getDateReceived());
    }

    public void validateId(String id) throws ValidationException {
        if (!id.matches("[A-Za-z0-9]+")){
            throw new ValidationException("Id-ul nu este alfanumeric.");
        }
    }

    public void validateDescriere(String name) throws ValidationException {
        if (!name.matches("[A-Za-z0-9.,;?/': ]+")){
            throw new ValidationException("Descrierea nu este valida.");
        }
    }

    public void validateDate(Integer deadline) throws ValidationException {
        if (deadline > 14 || deadline < 0){
            throw new ValidationException("Deadline-ul este invalid.");
        }
    }

    public void validateDeadline(Integer deadline, Integer dateReceived) throws ValidationException {
        if (deadline <= dateReceived) {
            throw new ValidationException("Data deadline-ului este mai mica decat data in care a fost primita");
        }
    }
}
