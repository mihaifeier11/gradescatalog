package Repository;


import Domain.Strategy;
import Domain.Tema;
import Exceptions.ValidationException;
import Validator.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TemaRepository implements CrudRepository<String, Tema> {
    Map<String, Tema> repository = new HashMap();
    IAEValidator nullValidator = new NullValidator();
    VEValidator temaValidator = new StrategyValidator().validator(Strategy.Tema);


    @Override
    public Tema findOne(String id) throws IllegalArgumentException {
        nullValidator.validate(id);
        return repository.get(id);
    }

    @Override
    public Collection<Tema> findAll() {
        return repository.values();
    }

    @Override
    public Tema save(Tema tema) throws ValidationException {
        nullValidator.validate(tema);
        temaValidator.validate(tema);

        String id = tema.getID();
        if (repository.get(id) == null) {
            repository.put(id, tema);
            return null;
        }
        return tema;
    }

    @Override
    public Tema delete(String id) {
        nullValidator.validate(id);
        if (repository.get(id) == null) {
            return null;
        } else {
            return repository.remove(id);
        }
    }

    @Override
    public Tema update(Tema tema) throws ValidationException {
        nullValidator.validate(tema);
        temaValidator.validate(tema);

        Object id = tema.getID();
        Tema ob = repository.get(id);
        if (ob != null) {
            ob.setDeadline(tema.getDeadline());
            return null;
        }
        return tema;
    }
}

