package Repository;

import Domain.Strategy;
import Domain.Student;
import Domain.Tema;
import Exceptions.ValidationException;
import Validator.IAEValidator;
import Validator.NullValidator;
import Validator.StrategyValidator;
import Validator.VEValidator;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TemaFileRepository implements CrudRepository{
    IAEValidator nullValidator = new NullValidator();
    VEValidator temaValidator = new StrategyValidator().validator(Strategy.Tema);
    @Override
    public Object findOne(Object o) {
        nullValidator.validate(o);
        List<Student> allTeme = new ArrayList<>();
        Tema tempTema;
        try{
            File fXmlFile = new File("src/Repository/tema.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);


            NodeList nList = doc.getElementsByTagName("tema");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);


                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    if (eElement.getAttribute("id").equals(o)) {


                        String id = eElement.getAttribute("id");
                        String descriere = eElement.getElementsByTagName("descriere").item(0).getTextContent();
                        Integer deadline = Integer.parseInt(eElement.getElementsByTagName("deadline").item(0).getTextContent());
                        Integer dateReceived = Integer.parseInt(eElement.getElementsByTagName("dateReceived").item(0).getTextContent());

                        tempTema = new Tema(id, descriere, deadline, dateReceived);

                        tempTema = new Tema(id, descriere, deadline, dateReceived);
                        return tempTema;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable findAll() {
        List<Tema> allTeme = new ArrayList<>();
        Tema tempTema;
        try{
            File fXmlFile = new File("src/Repository/tema.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);


            NodeList nList = doc.getElementsByTagName("tema");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);


                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    String id = eElement.getAttribute("id");
                    String descriere = eElement.getElementsByTagName("descriere").item(0).getTextContent();
                    Integer deadline = Integer.parseInt(eElement.getElementsByTagName("deadline").item(0).getTextContent());
                    Integer dateReceived = Integer.parseInt(eElement.getElementsByTagName("dateReceived").item(0).getTextContent());

                    tempTema = new Tema(id, descriere, deadline, dateReceived);
                    allTeme.add(tempTema);
                }
            }
            return allTeme;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public Object save(Object entity) throws ValidationException {
        nullValidator.validate(entity);

        if (findOne(((Tema) entity).getID()) != null){
            return entity;
        }

        List<Tema> allTeme = new ArrayList();
        allTeme = (List<Tema>) findAll();
        String xmlFilePath = new String("src/Repository/tema.xml");
        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            Document document = documentBuilder.newDocument();

            // root element
            Element root = document.createElement("teme");
            document.appendChild(root);

            Element tema = document.createElement("tema");

            for (Tema hw : allTeme){
                root.appendChild(tema);

                Attr attr = document.createAttribute("id");
                attr.setValue(hw.getID());
                tema.setAttributeNode(attr);

                Element descriere = document.createElement("descriere");
                descriere.appendChild(document.createTextNode(hw.getDescription()));
                tema.appendChild(descriere);

                Element deadline = document.createElement("deadline");
                deadline.appendChild(document.createTextNode(Integer.toString(hw.getDeadline())));
                tema.appendChild(deadline);

                Element dateReceived = document.createElement("dateReceived");
                dateReceived.appendChild(document.createTextNode(Integer.toString(hw.getDateReceived())));
                tema.appendChild(dateReceived);

                tema = document.createElement("tema");
            }

            Tema hw = (Tema) entity;

            root.appendChild(tema);

            Attr attr = document.createAttribute("id");
            attr.setValue(hw.getID());
            tema.setAttributeNode(attr);

            Element descriere = document.createElement("descriere");
            descriere.appendChild(document.createTextNode(hw.getDescription()));
            tema.appendChild(descriere);

            Element deadline = document.createElement("deadline");
            deadline.appendChild(document.createTextNode(Integer.toString(hw.getDeadline())));
            tema.appendChild(deadline);

            Element dateReceived = document.createElement("dateReceived");
            dateReceived.appendChild(document.createTextNode(Integer.toString(hw.getDateReceived())));
            tema.appendChild(dateReceived);


            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
        return null;
    }

    // TODO: TEST THIS
    @Override
    public Object delete(Object o) {
        nullValidator.validate(o);

        List<Tema> allTeme = new ArrayList();
        allTeme = (List<Tema>) findAll();
        String xmlFilePath = new String("src/Repository/tema.xml");

        boolean exists = false;
        Tema removedHW = null;
        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            Document document = documentBuilder.newDocument();

            // root element
            Element root = document.createElement("teme");
            document.appendChild(root);

            Element tema = document.createElement("tema");

            for (Tema hw : allTeme){
                if (!hw.getID().equals(o)) {
                    root.appendChild(tema);

                    Attr attr = document.createAttribute("id");
                    attr.setValue(hw.getID());
                    tema.setAttributeNode(attr);

                    Element descriere = document.createElement("descriere");
                    descriere.appendChild(document.createTextNode(hw.getDescription()));
                    tema.appendChild(descriere);

                    Element deadline = document.createElement("deadline");
                    deadline.appendChild(document.createTextNode(Integer.toString(hw.getDeadline())));
                    tema.appendChild(deadline);

                    Element dateReceived = document.createElement("dateReceived");
                    dateReceived.appendChild(document.createTextNode(Integer.toString(hw.getDateReceived())));
                    tema.appendChild(dateReceived);

                    tema = document.createElement("tema");
                } else {
                    exists = true;
                    removedHW = hw;
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            transformer.transform(domSource, streamResult);

            if (exists){
                return removedHW;
            } else {
                return null;
            }

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
        return null;
    }

    @Override
    public Object update(Object entity) throws ValidationException {
        nullValidator.validate(entity);

        if (findOne(((Tema) entity).getID()) == null){
            return entity;
        }

        Tema homew = (Tema) entity;

        List<Tema> allTeme = new ArrayList();
        allTeme = (List<Tema>) findAll();
        String xmlFilePath = new String("src/Repository/tema.xml");

        boolean exists = false;

        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            Document document = documentBuilder.newDocument();

            // root element
            Element root = document.createElement("teme");
            document.appendChild(root);

            Element tema = document.createElement("tema");
            for (Tema hw : allTeme){
                if (!(hw.getID().equals(homew.getID()))) {
                    root.appendChild(tema);

                    Attr attr = document.createAttribute("id");
                    attr.setValue(hw.getID());
                    tema.setAttributeNode(attr);

                    Element descriere = document.createElement("descriere");
                    descriere.appendChild(document.createTextNode(hw.getDescription()));
                    tema.appendChild(descriere);

                    Element deadline = document.createElement("deadline");
                    deadline.appendChild(document.createTextNode(Integer.toString(hw.getDeadline())));
                    tema.appendChild(deadline);

                    Element dateReceived = document.createElement("dateReceived");
                    dateReceived.appendChild(document.createTextNode(Integer.toString(hw.getDateReceived())));
                    tema.appendChild(dateReceived);

                    tema = document.createElement("tema");
                } else {
                    root.appendChild(tema);

                    Attr attr = document.createAttribute("id");
                    attr.setValue(homew.getID());
                    tema.setAttributeNode(attr);

                    Element descriere = document.createElement("descriere");
                    descriere.appendChild(document.createTextNode(homew.getDescription()));
                    tema.appendChild(descriere);

                    Element deadline = document.createElement("deadline");
                    deadline.appendChild(document.createTextNode(Integer.toString(homew.getDeadline())));
                    tema.appendChild(deadline);

                    Element dateReceived = document.createElement("dateReceived");
                    dateReceived.appendChild(document.createTextNode(Integer.toString(homew.getDateReceived())));
                    tema.appendChild(dateReceived);

                    tema = document.createElement("tema");
                    exists = true;
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

        if (exists) {
            return null;
        } else {
            return homew;
        }
    }

    public void clearFile(File file) {
        FileWriter writer;
        try {
            writer = new FileWriter(file);
            writer.write("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
