package Repository;

import Domain.Strategy;
import Domain.Student;
import Domain.Tema;
import Exceptions.ValidationException;
import Validator.IAEValidator;
import Validator.NullValidator;
import Validator.StrategyValidator;
import Validator.VEValidator;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TemaFileRepository implements CrudRepository{
    IAEValidator nullValidator = new NullValidator();
    VEValidator temaValidator = new StrategyValidator().validator(Strategy.Tema);
    @Override
    public Object findOne(Object o) {
        nullValidator.validate(o);
        File file = new File("src/Repository/tema.txt");
        BufferedReader reader = null;
        List<String> textTema =  new ArrayList<String>();
        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;

            while ((text = reader.readLine()) != null) {
                textTema = Arrays.asList(text.split(", "));
                if (o.equals(textTema.get(0))){
                    String id = textTema.get(0);
                    String descriere = textTema.get(1);
                    Integer deadline = Integer.parseInt(textTema.get(2));
                    Integer dateReceived = Integer.parseInt(textTema.get(3));
                    return new Tema(id, descriere, deadline, dateReceived);
                }
            }
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {

            }
        }
        return null;
    }

    @Override
    public Iterable findAll() {
        File file = new File("src/Repository/tema.txt");
        BufferedReader reader = null;
        List<String> textTema =  new ArrayList<>();
        List<Tema> allHW = new ArrayList<>();
        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;

            while ((text = reader.readLine()) != null) {
                textTema = Arrays.asList(text.split(", "));
                String id = textTema.get(0);
                String descriere = textTema.get(1);
                Integer deadline = Integer.parseInt(textTema.get(2));
                Integer dateReceived = Integer.parseInt(textTema.get(3));
                allHW.add(new Tema(id, descriere, deadline, dateReceived));

            }
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {

            }
        }
        return allHW;
    }

    @Override
    public Object save(Object entity) throws ValidationException {
        nullValidator.validate(entity);
        if (findOne(((Tema) entity).getID()) != null){
            return entity;
        }

        File file = new File("src/Repository/tema.txt");
        try {
            FileWriter fr = new FileWriter(file, true);

            fr.write(entity.toString());
            fr.close();
        } catch (IOException e) {}
        return null;
    }

    @Override
    public Object delete(Object o) {
        nullValidator.validate(o);
        File file = new File("src/Repository/tema.txt");
        BufferedReader reader = null;
        List<String> textTema = new ArrayList<String>();
        boolean exists = false;
        List<Tema> allHW = new ArrayList<Tema>();
        Tema removedTema = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;

            while ((text = reader.readLine()) != null) {

                textTema = Arrays.asList(text.split(", "));
                String id = textTema.get(0);
                String descriere = textTema.get(1);
                Integer deadline = Integer.parseInt(textTema.get(2));
                Integer dateReceived = Integer.parseInt(textTema.get(3));
                Tema tema = new Tema(id, descriere, deadline, dateReceived);

                if (!o.equals(textTema.get(0))) {
                    allHW.add(tema);
                } else {
                    exists = true;
                    removedTema = tema;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {

            }
        }
        FileWriter writer = null;
        clearFile(file);

        for (Tema hw : allHW){
            try {
                save(hw);
            } catch (ValidationException e) {
            }
        }
        if (exists == false) {
            return null;
        } else {
            return removedTema;
        }
    }

    @Override
    public Object update(Object entity) throws ValidationException {
        nullValidator.validate(entity);
        File file = new File("src/Repository/tema.txt");
        BufferedReader reader = null;
        List<String> textTema = new ArrayList<String>();
        boolean exists = false;
        List<Tema> allHW = new ArrayList<Tema>();
        Student removedTema = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;

            while ((text = reader.readLine()) != null) {

                textTema = Arrays.asList(text.split(", "));
                String id = textTema.get(0);
                String descriere = textTema.get(1);
                Integer deadline = Integer.parseInt(textTema.get(2));
                Integer dateReceived = Integer.parseInt(textTema.get(3));
                Tema tema = new Tema(id, descriere, deadline, dateReceived);
                String temaId = ((Tema) entity).getID();
                if (!temaId.equals(id)) {
                    allHW.add(tema);
                } else {
                    exists = true;
                    allHW.add((Tema)entity);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {

            }
        }
        FileWriter writer = null;
        clearFile(file);

        for (Tema hw : allHW){
            try {
                save(hw);
            } catch (ValidationException e) {
            }
        }
        if (exists == true) {
            return null;
        } else {
            return entity;
        }
    }

    public void clearFile(File file) {
        FileWriter writer;
        try {
            writer = new FileWriter(file);
            writer.write("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
