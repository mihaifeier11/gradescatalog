package Repository;

import Domain.Strategy;
import Domain.Student;
import Exceptions.ValidationException;
import Validator.IAEValidator;
import Validator.NullValidator;
import Validator.StrategyValidator;
import Validator.VEValidator;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StudentFileRepository implements CrudRepository{
    IAEValidator nullValidator = new NullValidator();
    @Override
    public Object findOne(Object o) {
        nullValidator.validate(o);
        File file = new File("src/Repository/student.txt");
        BufferedReader reader = null;
        List<String> textStudent =  new ArrayList<String>();
        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;

            while ((text = reader.readLine()) != null) {
                textStudent = Arrays.asList(text.split(", "));

                if (o.equals(textStudent.get(0))){
                    String id = textStudent.get(0);
                    String nume = textStudent.get(1);
                    Integer grupa = Integer.parseInt(textStudent.get(2));
                    String email = textStudent.get(3);
                    String cadruIndrumator = textStudent.get(4);
                    reader.close();
                    return new Student(id, nume, grupa, email, cadruIndrumator);
                }
            }
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {

            }
        }
        return null;
    }

    @Override
    public Iterable findAll() {
        File file = new File("src/Repository/student.txt");
        BufferedReader reader = null;
        List<String> textStudent =  new ArrayList<>();
        List<Student> allStudents = new ArrayList<>();
        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;

            while ((text = reader.readLine()) != null) {
                textStudent = Arrays.asList(text.split(", "));
                String id = textStudent.get(0);
                String nume = textStudent.get(1);
                Integer grupa = Integer.parseInt(textStudent.get(2));
                String email = textStudent.get(3);
                String cadruIndrumator = textStudent.get(4);
                allStudents.add(new Student(id, nume, grupa, email, cadruIndrumator));

            }
        } catch(FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {

            }
        }
        return allStudents;
    }

    @Override
    public Object save(Object entity) throws ValidationException {
        nullValidator.validate(entity);
        if (findOne(((Student) entity).getID()) != null){
            return entity;
        }

        File file = new File("src/Repository/student.txt");
        try {
            FileWriter fr = new FileWriter(file, true);

            fr.write(((Student) entity).toString());
            fr.close();
        }catch (IOException e) {}
        return null;
    }

    @Override
    public Object delete(Object o) {
        nullValidator.validate(o);
        File file = new File("src/Repository/student.txt");
        BufferedReader reader = null;
        List<String> textStudent = new ArrayList<String>();
        boolean exists = false;
        List<Student> allStudents = new ArrayList<Student>();
        Student removedStudent = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;

            while ((text = reader.readLine()) != null) {

                textStudent = Arrays.asList(text.split(", "));
                String id = textStudent.get(0);
                String nume = textStudent.get(1);
                Integer grupa = Integer.parseInt(textStudent.get(2));
                String email = textStudent.get(3);
                String cadruIndrumator = textStudent.get(4);
                Student student = new Student(id, nume, grupa, email, cadruIndrumator);

                if (!o.equals(textStudent.get(0))) {
                    allStudents.add(student);
                } else {
                    exists = true;
                    removedStudent = student;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {

            }
        }
        FileWriter writer = null;
        clearFile(file);

        for (Student st : allStudents){
            try {
                save(st);
            } catch (ValidationException e) {
            }
        }
        if (exists == false) {
            return null;
        } else {
            return removedStudent;
        }
    }

    @Override
    public Object update(Object entity) throws ValidationException {
        nullValidator.validate(entity);
        File file = new File("src/Repository/student.txt");
        BufferedReader reader = null;
        List<String> textStudent = new ArrayList<String>();
        boolean exists = false;
        List<Student> allStudents = new ArrayList<Student>();
        Student removedStudent = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;

            while ((text = reader.readLine()) != null) {

                textStudent = Arrays.asList(text.split(", "));
                String id = textStudent.get(0);
                String nume = textStudent.get(1);
                Integer grupa = Integer.parseInt(textStudent.get(2));
                String email = textStudent.get(3);
                String cadruIndrumator = textStudent.get(4);
                Student student = new Student(id, nume, grupa, email, cadruIndrumator);
                String studentId = ((Student) entity).getID();
                if (!studentId.equals(id)) {
                    allStudents.add(student);
                } else {
                    exists = true;
                    allStudents.add((Student)entity);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {

            }
        }
        FileWriter writer = null;
        clearFile(file);

        for (Student st : allStudents){
            try {
                save(st);
            } catch (ValidationException e) {
            }
        }
        if (exists == true) {
            return null;
        } else {
            return entity;
        }
    }

    public void clearFile(File file) {
        FileWriter writer;
        try {
            writer = new FileWriter(file);
            writer.write("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
