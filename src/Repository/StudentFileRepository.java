package Repository;

import Domain.Strategy;
import Domain.Student;
import Exceptions.ValidationException;
import Validator.IAEValidator;
import Validator.NullValidator;
import Validator.StrategyValidator;
import Validator.VEValidator;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.NodeList;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("ALL")
public class StudentFileRepository implements CrudRepository{
    IAEValidator nullValidator = new NullValidator();

    @Override
    public Object findOne(Object o) {
        nullValidator.validate(o);
        List<Student> allStudents = new ArrayList<>();
        Student tempStudent;
        try{
            File fXmlFile = new File("src/Repository/student.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);


            NodeList nList = doc.getElementsByTagName("student");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);


                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    if (eElement.getAttribute("id").equals(o)) {

                        String id = eElement.getAttribute("id");
                        String nume = eElement.getElementsByTagName("nume").item(0).getTextContent();
                        Integer grupa = Integer.parseInt(eElement.getElementsByTagName("grupa").item(0).getTextContent());
                        String email = eElement.getElementsByTagName("email").item(0).getTextContent();
                        String cadruIndrumator = eElement.getElementsByTagName("cadruIndrumator").item(0).getTextContent();

                        tempStudent = new Student(id, nume, grupa, email, cadruIndrumator);
                        return tempStudent;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public Iterable findAll() {
        List<Student> allStudents = new ArrayList<>();
        Student tempStudent;
        try{
            File fXmlFile = new File("src/Repository/student.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);


            NodeList nList = doc.getElementsByTagName("student");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);


                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    String id = eElement.getAttribute("id");
                    String nume = eElement.getElementsByTagName("nume").item(0).getTextContent();
                    Integer grupa = Integer.parseInt(eElement.getElementsByTagName("grupa").item(0).getTextContent());
                    String email = eElement.getElementsByTagName("email").item(0).getTextContent();
                    String cadruIndrumator = eElement.getElementsByTagName("cadruIndrumator").item(0).getTextContent();

                    tempStudent = new Student(id, nume, grupa, email, cadruIndrumator);
                    allStudents.add(tempStudent);
                }
            }
            return allStudents;
        } catch (Exception e) {
            e.printStackTrace();
    }
        return null;
    }


    @Override
    public Object save(Object entity) {
        nullValidator.validate(entity);

        if (findOne(((Student) entity).getID()) != null){
            return entity;
        }

        List<Student> allStudents = new ArrayList();
        allStudents = (List<Student>) findAll();
        String xmlFilePath = new String("src/Repository/student.xml");
        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            Document document = documentBuilder.newDocument();

            // root element
            Element root = document.createElement("studenti");
            document.appendChild(root);

            Element student = document.createElement("student");

            for (Student st : allStudents){
                root.appendChild(student);

                Attr attr = document.createAttribute("id");
                attr.setValue(st.getID());
                student.setAttributeNode(attr);

                Element name = document.createElement("nume");
                name.appendChild(document.createTextNode(st.getNume()));
                student.appendChild(name);

                Element grupa = document.createElement("grupa");
                grupa.appendChild(document.createTextNode(Integer.toString(st.getGrupa())));
                student.appendChild(grupa);

                Element email = document.createElement("email");
                email.appendChild(document.createTextNode(st.getEmail()));
                student.appendChild(email);

                Element cadruIndrumator = document.createElement("cadruIndrumator");
                cadruIndrumator.appendChild(document.createTextNode(st.getCadruIndrumator()));
                student.appendChild(cadruIndrumator);

                student = document.createElement("student");
            }

            Student st = (Student) entity;

            root.appendChild(student);
            Attr attr = document.createAttribute("id");
            attr.setValue(st.getID());
            student.setAttributeNode(attr);

            Element name = document.createElement("nume");
            name.appendChild(document.createTextNode(st.getNume()));
            student.appendChild(name);

            Element grupa = document.createElement("grupa");
            grupa.appendChild(document.createTextNode(Integer.toString(st.getGrupa())));
            student.appendChild(grupa);

            Element email = document.createElement("email");
            email.appendChild(document.createTextNode(st.getEmail()));
            student.appendChild(email);

            Element cadruIndrumator = document.createElement("cadruIndrumator");
            cadruIndrumator.appendChild(document.createTextNode(st.getCadruIndrumator()));
            student.appendChild(cadruIndrumator);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public Object delete(Object o) {
        nullValidator.validate(o);

        List<Student> allStudents = new ArrayList();
        allStudents = (List<Student>) findAll();
        String xmlFilePath = new String("src/Repository/student.xml");

        boolean exists = false;
        Student removedStudent = null;
        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            Document document = documentBuilder.newDocument();

            // root element
            Element root = document.createElement("studenti");
            document.appendChild(root);

            Element student = document.createElement("student");

            for (Student st : allStudents){
                if (!st.getID().equals(o)) {
                    root.appendChild(student);

                    Attr attr = document.createAttribute("id");
                    attr.setValue(st.getID());
                    student.setAttributeNode(attr);

                    Element name = document.createElement("nume");
                    name.appendChild(document.createTextNode(st.getNume()));
                    student.appendChild(name);

                    Element grupa = document.createElement("grupa");
                    grupa.appendChild(document.createTextNode(Integer.toString(st.getGrupa())));
                    student.appendChild(grupa);

                    Element email = document.createElement("email");
                    email.appendChild(document.createTextNode(st.getEmail()));
                    student.appendChild(email);

                    Element cadruIndrumator = document.createElement("cadruIndrumator");
                    cadruIndrumator.appendChild(document.createTextNode(st.getCadruIndrumator()));
                    student.appendChild(cadruIndrumator);

                    student = document.createElement("student");
                } else {
                    exists = true;
                    removedStudent = st;
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            transformer.transform(domSource, streamResult);

            if (exists){
                return removedStudent;
            } else {
                return null;
            }

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
        return null;
    }

    @Override
    public Object update(Object entity) throws ValidationException {
        nullValidator.validate(entity);

        if (findOne(((Student) entity).getID()) == null){
            return entity;
        }

        Student stud = (Student) entity;

        List<Student> allStudents = new ArrayList();
        allStudents = (List<Student>) findAll();
        String xmlFilePath = new String("src/Repository/student.xml");

        boolean exists = false;

        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            Document document = documentBuilder.newDocument();

            // root element
            Element root = document.createElement("studenti");
            document.appendChild(root);

            Element student = document.createElement("student");
            for (Student st : allStudents){
                if (!(st.getID().equals(stud.getID()))) {
                    root.appendChild(student);

                    Attr attr = document.createAttribute("id");
                    attr.setValue(st.getID());
                    student.setAttributeNode(attr);

                    Element name = document.createElement("nume");
                    name.appendChild(document.createTextNode(st.getNume()));
                    student.appendChild(name);

                    Element grupa = document.createElement("grupa");
                    grupa.appendChild(document.createTextNode(Integer.toString(st.getGrupa())));
                    student.appendChild(grupa);

                    Element email = document.createElement("email");
                    email.appendChild(document.createTextNode(st.getEmail()));
                    student.appendChild(email);

                    Element cadruIndrumator = document.createElement("cadruIndrumator");
                    cadruIndrumator.appendChild(document.createTextNode(st.getCadruIndrumator()));
                    student.appendChild(cadruIndrumator);

                    student = document.createElement("student");
                } else {
                    root.appendChild(student);
                    Attr attr = document.createAttribute("id");
                    attr.setValue(stud.getID());
                    student.setAttributeNode(attr);

                    Element name = document.createElement("nume");
                    name.appendChild(document.createTextNode(stud.getNume()));
                    student.appendChild(name);

                    Element grupa = document.createElement("grupa");
                    grupa.appendChild(document.createTextNode(Integer.toString(stud.getGrupa())));
                    student.appendChild(grupa);

                    Element email = document.createElement("email");
                    email.appendChild(document.createTextNode(stud.getEmail()));
                    student.appendChild(email);

                    Element cadruIndrumator = document.createElement("cadruIndrumator");
                    cadruIndrumator.appendChild(document.createTextNode(stud.getCadruIndrumator()));
                    student.appendChild(cadruIndrumator);

                    student = document.createElement("student");
                    exists = true;
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

        if (exists) {
            return null;
        } else {
            return stud;
        }

    }


    public void clearFile(File file) {
        FileWriter writer;
        try {
            writer = new FileWriter(file);
            writer.write("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

interface Eq{
    Object f(Student student);
}
