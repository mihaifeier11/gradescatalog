package Repository;

import Domain.HasID;
import Domain.Strategy;
import Domain.Student;
import Exceptions.ValidationException;
import Validator.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class StudentRepository implements CrudRepository<String, Student>{

    Map<String, Student> repository = new HashMap();
    IAEValidator nullValidator = new NullValidator();
    VEValidator studentValidator = new StrategyValidator().validator(Strategy.Student);

    @Override
    public Student findOne(String id) throws IllegalArgumentException{
        nullValidator.validate(id);
        return repository.get(id);
    }


    @Override
    public Collection<Student> findAll() {
        return repository.values();
    }


    @Override
    public Student save(Student entity) throws ValidationException {
        nullValidator.validate(entity);
        studentValidator.validate(entity);
        String id = entity.getID();
        if (repository.get(id) == null) {
            repository.put(id, entity);
            return null;
        }
        return entity;
    }


    @Override
    public Student delete(String id) {
        nullValidator.validate(id);
        if (repository.get(id) == null) {
            return null;
        } else {
            return repository.remove(id);
        }
    }


    @Override
    public Student update(Student entity) throws ValidationException {
        nullValidator.validate(entity);
        studentValidator.validate(entity);
        String id = entity.getID();
        if (repository.get(id) != null) {
            repository.put(id, entity);
            return null;
        }
        return entity;
    }


}
