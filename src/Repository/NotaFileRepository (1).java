package Repository;

import Domain.Nota;
import Exceptions.ValidationException;
import Validator.IAEValidator;
import Validator.NullValidator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class NotaFileRepository implements CrudRepository<String, Nota> {
    IAEValidator nullValidator = new NullValidator();
    @Override
    public Nota findOne(String s) {
        return null;
    }

    @Override
    public Iterable<Nota> findAll() {
        return null;
    }

    @Override
    public Nota save(Nota entity) throws ValidationException {
        nullValidator.validate(entity);
        String pathname = "src/Repository/note/" + entity.getStudent().getID() + ".txt";
        System.out.println(pathname);
        File file = new File(pathname);
        try {
            FileWriter fr = new FileWriter(file, true);

            fr.write(entity.toString());
            fr.close();
        }catch (IOException e) {}
        return null;
    }

    @Override
    public Nota delete(String s) {
        return null;
    }

    @Override
    public Nota update(Nota entity) throws ValidationException {
        return null;
    }
}
