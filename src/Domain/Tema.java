package Domain;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Tema implements HasID<String> {
    private String id;
    private String description;
    private int deadline;
    private int dateReceived;

    /**
     * the function creates a task
     * @param id
     *      the id of the task
     *      must be String
     * @param description
     *      the description of the task
     *      must be String
     * @param deadline
     *      the deadline of the task
     *      must be Date
     * @param dateReceived
     *      the date the task was received
     *      must be Date
     */
    public Tema(String id, String description, int deadline, int dateReceived) {
        this.id = id;
        this.description = description;
        this.deadline = deadline;
        this.dateReceived = dateReceived;
    }

    /**
     *
     * @return id - the id of the task
     */
    public String getID() {
        return id;
    }

    /**
     * the function sets the id of the task to the one given
     * @param id
     *      the new id of the task
     *      must be String
     */
    public void setID(String id) {
        this.id = id;
    }

    /**
     *
     * @return description - the description of the task
     */
    public String getDescription() {
        return description;
    }

    /**
     * the function sets the description of the task to the one given
     * @param description
     *      the new description
     *      must be String
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return deadline - the deadline of the task
     */
    public int getDeadline() {
        return deadline;
    }

    /**
     * the function sets the deadline of the task to the one given
     * @param deadline
     *      the new deadline
     *      must be Date
     */
    public void setDeadline(int deadline) {
        this.deadline = deadline;
    }

    /**
     *
     * @return dateReceived - the date the task was received
     */
    public int getDateReceived() {
        return dateReceived;
    }

    /**
     * the function sets the date the task was received to the one given
     * @param dateReceived
     *      the new date
     *      must be Date
     *      must be after current date
     */
    public void setDateReceived(int dateReceived) {

        this.dateReceived = dateReceived;
    }

    @Override
    public String toString() {

        return id +
                ", " + description  +
                ", " + deadline +
                ", " + dateReceived +
                "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tema tema = (Tema) o;
        return Objects.equals(id, tema.id) &&
                Objects.equals(description, tema.description) &&
                Objects.equals(deadline, tema.deadline) &&
                Objects.equals(dateReceived, tema.dateReceived);
    }

}
