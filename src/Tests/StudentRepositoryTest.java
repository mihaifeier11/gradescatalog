package Tests;

import Domain.Student;
import Exceptions.ValidationException;
import Repository.CrudRepository;
import Repository.StudentRepository;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class StudentRepositoryTest {
    StudentRepository repository = new StudentRepository();
    Student student1 = new Student("1", "nume", 213, "email1@email.com", "profesor");
    Student student2 = new Student("2", "nume", 213, "email2@email.com", "profesor");
    Student student3 = new Student("3", "nume", 213, "email3@email.com", "profesor");
    Student student4 = new Student("1", "nume", 213, "email2@email.com", "profesor");

    @Test
    void findOne() throws ValidationException {
        repository.save(student1);

        assertEquals(student1, repository.findOne("1"));
        assertEquals(null, repository.findOne("2"));

        try{
            repository.findOne(null);
            assertEquals(true, false);
        } catch (IllegalArgumentException ex){
            assertEquals(true, true);
        }

    }

    @Test
    void findAll() throws ValidationException {
        repository.save(student1);
        repository.save(student2);
        repository.save(student3);

        Collection<Student> allEntities = repository.findAll();
        assertEquals(true, allEntities.contains(student1));
        assertEquals(true, allEntities.contains(student2));
        assertEquals(true, allEntities.contains(student3));
        assertEquals(3, allEntities.size());
    }

    @Test
    void save() throws ValidationException {
        Object result = repository.save(student1);
        assertEquals(result,null);

        result = repository.save(student1);
        assertEquals(result, student1);

        try{
            repository.save(null);
            assertEquals(true, false);
        } catch (IllegalArgumentException ex){
            assertEquals(true, true);
        }


    }

    @Test
    void delete() throws ValidationException {
        repository.save(student1);
        assertEquals(repository.delete("1"), student1);

        assertEquals(repository.delete("1"), null);

        try{
            repository.delete(null);
            assertEquals(true, false);
        } catch (IllegalArgumentException ex){
            assertEquals(true, true);
        }
    }

    @Test
    void update() throws ValidationException {
        repository.save(student1);

        assertEquals(repository.update(student4), null);

        assertEquals(repository.findOne("1"), student4);

        assertEquals(repository.update(student2), student2);

        try{
            repository.update(null);
            assertEquals(true, false);
        } catch (IllegalArgumentException ex){
            assertEquals(true, true);
        }


    }
}