package Tests;

import Domain.Student;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {
    Student student = new Student("123", "Domain.Student", 223, "email@domeniu.com", "Profesor");
    Student student2 = new Student("111", "StudentModificat", 213, "email@test.com", "ProfesorTest");
    Student student3 = new Student("111", "StudentModificat", 213, "email@test.com", "ProfesorTest");

    @org.junit.jupiter.api.Test
    void testGetId() {
        assertEquals("123", student.getID());
    }

    @org.junit.jupiter.api.Test
    void testSetId() {
        student.setID("111");
        assertEquals("111", student.getID());
    }

    @org.junit.jupiter.api.Test
    void testGetNume() {
        assertEquals("Domain.Student", student.getNume());
    }

    @org.junit.jupiter.api.Test
    void testSetNume() {
        student.setNume("StudentModificat");
        assertEquals("StudentModificat", student.getNume());
    }

    @org.junit.jupiter.api.Test
    void testGetGrupa() {
        assertEquals(223, student.getGrupa());
    }

    @org.junit.jupiter.api.Test
    void testSetGrupa() {
        student.setGrupa(213);
        assertEquals(213, student.getGrupa());
    }

    @org.junit.jupiter.api.Test
    void testGetEmail() {
        assertEquals("email@domeniu.com", student.getEmail());
    }

    @org.junit.jupiter.api.Test
    void testSetEmail() {
        student.setEmail("email@test.com");
        assertEquals("email@test.com", student.getEmail());
    }

    @org.junit.jupiter.api.Test
    void testGetCadruIndrumator() {
        assertEquals("Profesor", student.getCadruIndrumator());
    }

    @org.junit.jupiter.api.Test
    void testSetCadruIndrumator() {
        student.setCadruIndrumator("ProfesorTest");
        assertEquals("ProfesorTest", student.getCadruIndrumator());
    }

    @org.junit.jupiter.api.Test
    void testToString() {
        final String expected = "Id: 123, Nume: Domain.Student, Grupa: 223, Email: email@domeniu.com, Cadru Indrumator: Profesor\n";
        assertEquals(expected, student.toString());
    }

    @org.junit.jupiter.api.Test
    void testEquals() {
        final boolean actual = student.equals(student2);
        assertEquals(false, actual);

        final boolean actual2 = student2.equals(student3);
        assertEquals(true, actual2);
    }
}