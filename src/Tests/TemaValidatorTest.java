package Tests;

import Domain.Tema;
import Exceptions.ValidationException;
import Validator.TemaValidator;
import Validator.VEValidator;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("ALL")
class TemaValidatorTest {
    VEValidator temaValidator = new TemaValidator();
    Tema tema = new Tema("1", "descriere", 4, 3);

    @Test
    void validate() {
        try{
            temaValidator.validate(tema);
        } catch (ValidationException ex) {
            assertEquals(true, false);
        }

        try{
            temaValidator.validate(null);
            assertEquals(true, false);
        } catch (ValidationException ex) {
            ;
        }
    }
}