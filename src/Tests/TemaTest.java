package Tests;

import Domain.Tema;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

@SuppressWarnings("ALL")
class TemaTest {
    Tema tema = new Tema("1", "descriere", 4, 3);
    Tema tema2 = new Tema("1", "descriere", 4, 3);
    Tema tema3 = new Tema("2", "descriere", 4, 3);


    @org.junit.jupiter.api.Test
    void getId() {
        String actual = tema.getID();
        String expected = "1";
        assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void setId() {
        tema.setID("2");
        String actual = tema.getID();
        String expected = "2";
        assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void getDescription() {
        String actual = tema.getDescription();
        String expected = "descriere";
        assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void setDescription() {
        tema.setDescription("descriere noua");
        String actual = tema.getDescription();
        String expected = "descriere noua";
        assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void getDeadline() {
        int actual = tema.getDeadline();
        assertEquals(4, actual);
    }

    @org.junit.jupiter.api.Test
    void setDeadline() {
        tema.setDeadline(3);
        int actual = tema.getDeadline();
        assertEquals(3, 3);
    }

    @org.junit.jupiter.api.Test
    void getDateReceived() {
        int actual = tema.getDateReceived();
        assertEquals(3, actual);
    }

    @org.junit.jupiter.api.Test
    void setDateReceived() {
        tema.setDateReceived(2);
        int actual = tema.getDateReceived();
        assertEquals(2, actual);
    }

    @org.junit.jupiter.api.Test
    void testToString() {
        String actual = tema.toString();
        String expected = "Id: 1, Descriere: descriere, Deadline: 4, Data primire: 3\n";
        assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void testEquals() {
        boolean actual = tema.equals(tema2);
        boolean expected = true;
        assertEquals(expected, actual);

        actual = tema.equals(tema3);
        expected = false;
        assertEquals(expected, actual);
    }
}