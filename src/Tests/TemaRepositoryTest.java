package Tests;

import Domain.Tema;
import Exceptions.ValidationException;
import Repository.TemaRepository;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("ALL")
class TemaRepositoryTest {
    TemaRepository repository = new TemaRepository();
    Tema tema1 = new Tema("1", "tema1", 4, 3);
    Tema tema2 = new Tema("2", "tema2", 4, 2);
    Tema tema3 = new Tema("3", "tema3", 4, 1);
    Tema tema4 = new Tema("1", "tema1", 5, 3);

    @Test
    void findOne() throws ValidationException {
        repository.save(tema1);

        assertEquals(tema1, repository.findOne("1"));
        assertEquals(null, repository.findOne("2"));

        try{
            repository.findOne(null);
            assertEquals(true, false);
        } catch (IllegalArgumentException ex){
            assertEquals(true, true);
        }

    }

    @Test
    void findAll() throws ValidationException {
        repository.save(tema1);
        repository.save(tema2);
        repository.save(tema3);

        Collection<Tema> allEntities = repository.findAll();
        assertEquals(true, allEntities.contains(tema1));
        assertEquals(true, allEntities.contains(tema2));
        assertEquals(true, allEntities.contains(tema3));
    }

    @Test
    void save() throws ValidationException {
        Object result = repository.save(tema1);
        assertEquals(result,null);

        result = repository.save(tema1);
        assertEquals(result, tema1);

        try{
            repository.save(null);
            assertEquals(true, false);
        } catch (IllegalArgumentException ex){
            assertEquals(true, true);
        }

    }

    @Test
    void delete() throws ValidationException {
        repository.save(tema1);
        assertEquals(repository.delete("1"), tema1);

        assertEquals(repository.delete("1"), null);

        try{
            repository.delete(null);
            assertEquals(true, false);
        } catch (IllegalArgumentException ex){
            assertEquals(true, true);
        }
    }

    @Test
    void update() throws ValidationException {
        repository.save(tema1);

        assertEquals(repository.update(tema4), null);

        assertEquals(repository.findOne("1"), tema4);

        assertEquals(repository.update(tema2), tema2);

        try{
            repository.update(null);
            assertEquals(true, false);
        } catch (IllegalArgumentException ex){
            assertEquals(true, true);
        }

    }
}