package Tests;

import Exceptions.ValidationException;
import Validator.DeadlineValidator;
import Validator.VEValidator;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("ALL")
class DeadlineValidatorTest {
    VEValidator deadlineValidator = new DeadlineValidator();
    @Test
    void validate() throws ValidationException {
        Date date = new Date(2018, 12, 12);
        try{
            deadlineValidator.validate(date);
            assertEquals(true, true);
        } catch (ValidationException ex){
            assertEquals(true, false);
        }

        date = new Date(2018, 1,1);
            try{
            deadlineValidator.validate(date);
            assertEquals(true, false);
        } catch (ValidationException ex){
            assertEquals(true, true);
        }
    }
}