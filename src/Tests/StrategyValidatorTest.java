package Tests;

import Domain.Strategy;
import Domain.Tema;
import Validator.StrategyValidator;
import Validator.StudentValidator;
import Validator.TemaValidator;
import Validator.VEValidator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StrategyValidatorTest {
    StrategyValidator strategyValidator = new StrategyValidator();

    @Test
    void validator() {
        boolean actual = strategyValidator.validator(Strategy.Student) instanceof StudentValidator;
        assertEquals(true, actual);

        actual = strategyValidator.validator(Strategy.Tema) instanceof TemaValidator;
        assertEquals(true, actual);
    }
}