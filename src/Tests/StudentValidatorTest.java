package Tests;

import Domain.Student;
import Exceptions.ValidationException;
import Validator.StudentValidator;
import Validator.VEValidator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentValidatorTest {
    StudentValidator studentValidator = new StudentValidator();
    Student student = new Student("123", "Domain.Student", 223, "email@domeniu.com", "Profesor");

    @Test
    void validate() {
        try{
            studentValidator.validate(student);
        } catch (ValidationException ex) {
            assertEquals(true, false);
        }

        try{
            studentValidator.validate(null);
            assertEquals(true, false);
        } catch (ValidationException ex) {
            ;
        }
    }

    @Test
    void validateId() throws ValidationException {
        try{
            studentValidator.validateId("123");
        } catch (ValidationException ex){
            assertEquals(true, false);
        }

        try{
            studentValidator.validateId("123@");
            assertEquals(true, false);
        } catch (ValidationException ex){
            ;
        }
    }

    @Test
    void validateName() throws ValidationException {
        try{
            studentValidator.validateName("Azasda ");
        } catch (ValidationException ex){
            assertEquals(true, false);
        }

        try{
            studentValidator.validateName("Azasda  @");
            assertEquals(true, false);
        } catch (ValidationException ex){
            ;
        }
    }

    @Test
    void validateEmail() throws ValidationException {
        try {
            studentValidator.validateEmail("nume@email.com");
        } catch (ValidationException ex) {
            assertEquals(true, false);
        }

        try {
            studentValidator.validateEmail("nume@emailcom@");
            assertEquals(true, false);
        } catch (ValidationException ex) {
            ;
        }
    }
}