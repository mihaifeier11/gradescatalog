package Tests;

import Validator.IAEValidator;
import Validator.NullValidator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NullValidatorTest {
    IAEValidator nullValidator = new NullValidator();

    @Test
    void validate() {
        try{
            nullValidator.validate("str");
        } catch (IllegalArgumentException ex){
            assertEquals(true, false);
        }

        try{
            nullValidator.validate(null);
            assertEquals(true, false);
        } catch (IllegalArgumentException ex){
            ;
        }

    }
}