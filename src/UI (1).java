import Domain.Student;
import Domain.Tema;
import Exceptions.ValidationException;
import Service.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class UI {
    static Service service = new Service();
    static Scanner scanner = new Scanner(System.in);
    Map<String, Runnable> menu = new HashMap<>();

    public void printCommands(){
        System.out.println("1. Adaugare student.");
        System.out.println("2. Modifica numele studentului.");
        System.out.println("3. Modifica grupa studentului.");
        System.out.println("4. Modifica email-ul studentului.");
        System.out.println("5. Modifica cadrul indrumator studentului.");
        System.out.println("6. Sterge un student.");
        System.out.println("7. Afiseaza un student.");
        System.out.println("8. Afiseaza toti studentii.");
        System.out.println("9. Adauga o tema.");
        System.out.println("10. Schimba deadline-ul unui teme.");
        System.out.println("11. Arata o tema.");
        System.out.println("12. Arata toate temele.");
        System.out.println("0. Iesire.");
    }

    public static void addStudent(){
        System.out.print("Id: ");
        String id = scanner.nextLine();
        System.out.print("Nume: ");
        String nume = scanner.nextLine();

        System.out.print("Grupa: ");
        Integer grupa = Integer.parseInt(scanner.nextLine());

        System.out.print("Email: ");
        String email = scanner.nextLine();

        System.out.print("Cadru Indrumator: ");
        String cadruIndrumator = scanner.nextLine();

        try {
            Object returnStatement = service.addStudent(id, nume, grupa, email, cadruIndrumator);
            if (returnStatement == null) {
                System.out.println("Studentul a fost salvat.");
            } else {
                System.out.println("Studentul exista deja.");
            }
        } catch (ValidationException e) {
            ;
        }
    }

    public static void modifyStudentName(){
        System.out.print("Id: ");
        String id = scanner.nextLine();
        System.out.print("Nume: ");
        String nume = scanner.nextLine();
        Object o = service.getStudent(id);
        if (o == null){
            System.out.println("Studentul cu acest id nu exista.");
        } else {
            ((Student) o).setNume(nume);
            try {
                o = service.updateStudent((Student)o);
                if (o == null){
                    System.out.println("Numele studentului a fost modificat cu succes.");
                }
            } catch (ValidationException e) {
                ;
            }
        }
    }

    public static void modifyStudentGroup() {
        System.out.print("Id: ");
        String id = scanner.nextLine();
        System.out.print("Grupa: ");
        Integer grupa = Integer.parseInt(scanner.nextLine());
        Object o = service.getStudent(id);
        if (o == null){
            System.out.println("Studentul cu acest id nu exista.");
        } else {
            ((Student) o).setGrupa(grupa);
            try {
                o = service.updateStudent((Student)o);
                if (o == null){
                    System.out.println("Grupa studentului a fost modificata cu succes.");
                }
            } catch (ValidationException e) {
                ;
            }
        }
    }

    public static void modifyStudentEmail() {
        System.out.print("Id: ");
        String id = scanner.nextLine();
        System.out.print("Email: ");
        String email = scanner.nextLine();
        Object o = service.getStudent(id);
        if (o == null){
            System.out.println("Studentul cu acest id nu exista.");
        } else {
            ((Student) o).setEmail(email);
            try {
                o = service.updateStudent((Student)o);
                if (o == null){
                    System.out.println("Email-ul studentului a fost modificat cu succes.");
                }
            } catch (ValidationException e) {
                ;
            }
        }
    }

    public static void modifyStudentCadruIndrumator() {
        System.out.print("Id: ");
        String id = scanner.nextLine();
        System.out.print("Cadru indrumator: ");
        String cadruIndrumator = scanner.nextLine();
        Object o = service.getStudent(id);
        if (o == null){
            System.out.println("Studentul cu acest id nu exista.");
        } else {
            ((Student) o).setCadruIndrumator(cadruIndrumator);
            try {
                o = service.updateStudent((Student)o);
                if (o == null){
                    System.out.println("Cadrul indrumator al studentului a fost modificat cu succes.");
                }
            } catch (ValidationException e) {
                ;
            }
        }
    }
    public static void deleteStudent(){
        System.out.print("Id: ");
        String id = scanner.nextLine();
        Object o = service.deleteStudent(id);
        if (o == null){
            System.out.println("Studentul nu exista.");
        } else {
            System.out.print("Studentul sters:");
            System.out.println(o);
        }

    }
    public static void showStudent(){
        System.out.print("Id: ");
        String id = scanner.nextLine();
        System.out.println(service.getStudent(id));
    }

    public static void showAllStudents(){
        ArrayList<Student> studentList = (ArrayList<Student>) service.getAllStudents();
        for (Student st : studentList){
            System.out.println(st);
        }
    }

    public static void addTema(){
        System.out.print("Id: ");
        String id = scanner.nextLine();

        System.out.print("Descriere: ");
        String descriere = scanner.nextLine();

        System.out.print("Deadline: ");
        Integer deadline = Integer.parseInt(scanner.nextLine());

        System.out.print("Data primire: ");
        Integer dateReceived = Integer.parseInt(scanner.nextLine());

        try {
            Object o = service.addTema(id, descriere, deadline, dateReceived);
            if (o == null){
                System.out.println("Tema a fost adaugata cu succes");
            } else {
                System.out.println("Tema exista deja.");
            }
        } catch (ValidationException e) {
            ;
        }
    }

    public static void setDeadline(){
        System.out.print("Id: ");
        String id = scanner.nextLine();

        System.out.print("Deadline: ");
        Integer deadline = Integer.parseInt(scanner.nextLine());

        try {
            Object o = service.setDeadline(id, deadline);
            if (o == null){
                System.out.println("Deadline-ul a fost modificat cu succes.");
            }
        } catch (ValidationException e) {
            ;
        }

    }

    public static void showTema(){
        System.out.print("Id: ");
        String id = scanner.nextLine();
        System.out.println(service.getTema(id));
    }

    public static void showAllTeme(){
        ArrayList<Tema> temaList = (ArrayList<Tema>) service.getAllTeme();
        for (Tema hw : temaList){
            System.out.println(hw);
        }
    }

    public static void addNota(){
        System.out.print("Id student: ");
        String idStudent = scanner.nextLine();

        System.out.print("Id tema: ");
        String idTema = scanner.nextLine();

        System.out.print("Nota: ");
        Integer nota = Integer.parseInt(scanner.nextLine());

        try {
            Object o = service.addNota(idStudent, idTema, nota);
            if (o == null){
                System.out.println("Tema a fost adaugata cu succes");
            } else {
                System.out.println("Tema exista deja.");
            }
        } catch (ValidationException e) {
            ;
        }
    }

    public static void setCurrentWeek(){
        System.out.print("Saptamana curenta: ");
        Integer currentWeek = Integer.parseInt(scanner.nextLine());
        service.setCurrentWeek(currentWeek);
    }

    public UI() {
        menu.put("1", UI::addStudent);
        menu.put("2", UI::modifyStudentName);
        menu.put("3", UI::modifyStudentGroup);
        menu.put("4", UI::modifyStudentEmail);
        menu.put("5", UI::modifyStudentCadruIndrumator);
        menu.put("6", UI::deleteStudent);
        menu.put("7", UI::showStudent);
        menu.put("8", UI::showAllStudents);
        menu.put("9", UI::addTema);
        menu.put("10", UI::setDeadline);
        menu.put("11", UI::showTema);
        menu.put("12", UI::showAllTeme);
        menu.put("13", UI::addNota);


        setCurrentWeek();
        printCommands();

        String command;
        while (true){
            System.out.print("Comanda: ");
            command = scanner.nextLine();
            menu.get(command).run();
//            switch (command) {
//                case "0":{
//                    System.out.println("O zi buna!");
//                    System.exit(0);
//                }
//                case "1":{
//                    addStudent();
//                    break;
//                }
//                case "2":{
//                    modifyStudentName();
//                    break;
//                }
//                case "3":{
//                    modifyStudentGroup();
//                    break;
//                }
//                case "4":{
//                    modifyStudentEmail();
//                    break;
//                }
//                case "5":{
//                    modifyStudentCadruIndrumator();
//                    break;
//                }
//                case "6":{
//                    deleteStudent();
//                    break;
//                }
//                case "7":{
//                    showStudent();
//                    break;
//                }
//                case "8":{
//                    showAllStudents();
//                    break;
//                }
//
//                case "9":{
//                    addTema();
//                    break;
//                }
//
//                case "10":{
//                    setDeadline();
//                    break;
//                }
//                case "11":{
//                    showTema();
//                    break;
//                }
//                case "12":{
//                    showAllTeme();
//                    break;
//                }
//
//            }
        }
    }
}
